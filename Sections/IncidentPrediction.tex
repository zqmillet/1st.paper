%!TEX root = ../Main.tex

\section{Multi-Model Based Incident Prediction}
\label{sec:Multi-Model Based Incident Prediction}
In this section, the relationship between atom attacks in multi-step attacks, the dependency of system functions and the causality of incidents are analyzed firstly. Then multiple domains knowledge are modelled into a multi-level Bayesian network. At last, a multi-model based incident prediction method will be introduced.

\subsection{Bayesian Network Based Knowledge Modelling}
\label{sec:Bayesian Network Based Knowledge Modelling}
Throughout the history of cyber attack of ICSs, it is noted that the purpose of attackers is to damage the control system. To achieve the destructive purpose, attackers must complete three steps: (1) infiltrate into field network, (2) invalid system functions, and (3) cause incidents. To predict the incidents, attacks and function failure must be considered, so we need to establish a model of the relationship between attacks, system functions and incidents.

Theoretically, what probabilistic inference needs is a joint probability distribution, but it suffers from an exponential complexity with the number of variables. There are various potential attacks, plenty of system functions and numbers of unanticipated incidents, so the joint probability distribution will be too large to be available. Bayesian network is developed to solve this problem, it can split the complicated joint probability distribution into a series of simple nodes, and as a result it reduces the difficulty of knowledge acquisition and the complexity of probabilistic inference. Bayesian network is widely used in fault diagnosis \cite{codetta-ra-2015-p13-24}, decision-theoretic troubleshooting \cite{heckerman-1995-p49-57}, etc.

There are three sorts of knowledge need to be converted into a multi-level Bayesian network which consists of four parts: attack level, function level, incident level and information transfer between levels. Next, we will describe the modelling procedure from these four parts.

\subsubsection{Attack Level}
\label{sec:Attack Level}
Cyber attacks are becoming increasingly complex, especially when the target is an ICS, characterized by a layered architecture that integrates several security technologies. These contexts can be violated by a multi-step attack that is a complex attack strategy that comprises multiple correlated atom attacks. To launch an atom attack, all conditions of this attack must be met. If an atom attack works, attacker will obtain some resources which may be the conditions of other atom attacks. The purpose of launching any atom attacks is to prepare for subsequent atom attacks. To describe the atom attacks of multi-step attack with Bayesian network, we propose two sorts of nodes: atom attack node and resource node.

In these paper, we use Bayesian network to describe relationships between attack nodes and resource nodes. There are two steps to generate a Bayesian network. Firstly, generate a DAG, secondly generate a conditional probability table for each node in DAG.

By vulnerability scanning, we can collect vulnerabilities of ICS. Then we enumerate all possible attack scenarios with the information of system vulnerabilities. Next, we analyze the conditions and results of each atom attack in attack scenarios. Assuming there are $m$ atom attacks and $n$ resources, we establish a $(m+n)\times(m+n)$ incidence matrix $[A_{i,j}]$. If conditions of atom attack $a_j$ are $r_{i1},r_{i2},\cdots,r_{ix}$, then let $A_{ik,j} = 1$, where $k = 1,2,\cdots,x$. If attacker can obtain resource $r_{j1},r_{j2},\cdots,r_{jy}$ by launching an atom attack $a_i$, then let $A_{i,jk} = 1$, where $k = 1,2,\cdots,y$. At last, we can generate a DAG with the incidence matrix $[A_{i,j}]$.

Assume there are $n$ resource nodes $r_1,r_2,\cdots,r_n$ point to the attack node $a_i$, in other words, attack node $a_i$ has $n$ parent nodes, Bayesian network adopts a conditional probability table to depict the condition of attack $a_i$, which is shown in the \cref{tab:The Condition of Attack a_i}.
\begin{table}[!ht]
    \centering
    \caption{The Condition of Attack $a_i$}\label{tab:The Condition of Attack a_i}
    \begin{tabu}to 0.85\textwidth{@{}X[-1,c]|*6{X[c]}@{}}
        $O(r_1)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        $O(r_2)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        \vd                 & \vd & \vd & \vd & \vd & $\ddots$ & \vd\\
        $O(r_n)$            & \TT & \FF & \TT & \TT & $\cdots$ & \FF\\\hline
        $C(a_i)$            & $c_{a_i,1}$ & $c_{a_i,2}$ & $c_{a_i,3}$ & $c_{a_i,4}$ & $\cdots$ & $c_{a_i,2^n}$\\
        $\overline{C}(a_i)$ & $1-c_{a_i,1}$ & $1-c_{a_i,2}$ & $1-c_{a_i,3}$ & $1-c_{a_i,4}$ & $\cdots$ & $1-c_{a_i,2^n}$\\
    \end{tabu}
\end{table}

In \cref{tab:The Condition of Attack a_i}, \TT{} means true and \FF{} means false, $C(a_i)$ represents that condition of $a_i$ is met, $\overline{C}(a_i)$ represents that condition of $a_i$ is not met, and $O(r_i)$ means that the resource $r_i$ has been obtained by attacker. $c_{a_i,x}$, where $x=1,2,\cdots,2^m$, is a boolean. If $c_{a_i,x} = 1$, it means the condition of $a_i$ is met, if $c_{a_i,x} = 0$, the condition is not met. For example, assuming $c_{a_i,1} = 1$, the first column of \cref{tab:The Condition of Attack a_i} shows that when attacker obtains all resources $r_1, r_2, \cdots, r_n$, the condition of launching attack $a_i$ is met. The values of $c_{a_i,x}$ are obtained by analyzing the attack knowledge of $a_i$.

In general, meeting the condition of an attack does not mean that the attacker must launch the attack, so Bayesian network uses the $\ell_{a_i}$ to measure the probability of launching attack $a_i$. The probability of launching attack $a_i$ is shown in \cref{tab:The Probabilities of Launching Attack a_i}.

\begin{table}[!ht]
    \centering
    \caption{The Probabilities of Launching Attack $a_i$}\label{tab:The Probabilities of Launching Attack a_i}
    \begin{tabu}to 0.47\textwidth{@{}X[-1,c]|X[2,c]X[2,c]@{}}
    $C(a_i)$            & \TT & \FF \\\hline
    $L(a_i)$            & $\ell_{a_i}$ & 0\\
    $\overline{L}(a_i)$ & $1-\ell_{a_i}$ & 1\\
    \end{tabu}
\end{table}

In \cref{tab:The Probabilities of Launching Attack a_i}, $L(a_i)$ means that attacker launches attack $a_i$, and $\overline{L}(a_i)$ represents that attack does not launch attack $a_i$. As we can see, if $\overline{C}(a_i)$, the attack has no chance to launch attack $a_i$. The value of $\ell_{a_i}$ is obtained from historical data or expertise.

To simplify the Bayesian network, \cref{tab:The Condition of Attack a_i} and \cref{tab:The Probabilities of Launching Attack a_i} can be merged into one table, which is shown in \cref{tab:The Conditional Probability Table of a_i}.

\begin{table}[!ht]
    \centering
    \caption{The Conditional Probability Table of $a_i$}\label{tab:The Conditional Probability Table of a_i}
    \begin{tabu}to 0.85\textwidth{@{}X[-1,c]|*6{X[c]}@{}}
        $O(r_1)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        $O(r_2)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        \vd                 & \vd & \vd & \vd & \vd & $\ddots$ & \vd\\
        $O(r_n)$            & \TT & \FF & \TT & \TT & $\cdots$ & \FF\\\hline
        $L(a_i)$            & $l_{a_i,1}$ & $l_{a_i,2}$ & $l_{a_i,3}$ & $l_{a_i,4}$ & $\cdots$ & $l_{a_i,2^n}$\\
        $\overline{L}(a_i)$ & $1-l_{a_i,1}$ & $1-l_{a_i,2}$ & $1-l_{a_i,3}$ & $1-l_{a_i,4}$ & $\cdots$ & $1-l_{a_i,2^n}$\\
    \end{tabu}
\end{table}
In \cref{tab:The Conditional Probability Table of a_i}, $l_{a_i,x} = \ell_{a_i}c_{a_i,x}$, where $x = 1,2,\cdots,2^n$.

Assume that the resource node $r_j$ has $m$ parent nodes $a_1, a_2, \cdots, a_m$, and it means when attacker launched some attacks in $a_1, a_2, \cdots, a_m$, he will have a chance to obtain the resource $r_j$. The probabilities of obtaining resource $r_j$ is shown in \cref{tab:The Probabilities of Obtaining Resource r_j}.
\begin{table}[!ht]
    \centering
    \caption{The Probabilities of Obtaining Resource $r_j$}\label{tab:The Probabilities of Obtaining Resource r_j}
    \begin{tabu}to 0.85\textwidth{@{}X[-1,c]|*6{X[c]}@{}}
        $L(a_1)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        $L(a_2)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        \vd                 & \vd & \vd & \vd & \vd & $\ddots$ & \vd\\
        $L(a_m)$            & \TT & \FF & \TT & \TT & $\cdots$ & \FF\\\hline
        $O(r_j)$            & $o_{r_j,1}$   & $o_{r_j,2}$ & $o_{r_j,3}$ & $o_{r_j,4}$ & $\cdots$ & $o_{r_j,2^m}$\\
        $\overline{O}(r_j)$ & $1-o_{r_j,1}$ & $1-o_{r_j,2}$ & $1-o_{r_j,3}$ & $1-o_{r_j,4}$ & $\cdots$ & $1-o_{r_j,2^m}$\\
    \end{tabu}
\end{table}

In \cref{tab:The Probabilities of Obtaining Resource r_j}, $O(r_j)$ means that resource $r_j$ is obtained by attacker, $\overline{O}(r_j)$ means that resource $r_j$ is not obtained by attacker. $o_{r_j,y}$, where $y = 1,2,\cdots,2^m$, is the probability of $O(r_j)$. The first column of \cref{tab:The Probabilities of Obtaining Resource r_j} shows that when attacker launches all attacks $a_1,a_2,\cdots,a_m$, the probability that he obtains resource $r_j$ is $o_{r_j,1}$. The values of $o_{r_j,y}$ are obtained from historical data or expertise.

\subsubsection{Function Level}
\label{sec:Function Level}
As we all know, ICSs are tight coupling systems, if a function is invalid, it may cause the failures of other functions. This phenomenon is called cascading failure. Fault tree analysis is used extensively to predict the cascading failure of control system \cite{volkanovsk-2009-p1116-1127, fajardo-2013-p260-272, cheng-2014-p1372-1381}. The main objectives of a fault tree analysis are: (1) To identify all possible combinations of basic events that may result in a critical event in the system. (2) To find the probability that the critical event will occur during a specified time interval or at a specified time $t$, or the frequency of the critical event. (3) To identify aspects of the system that need to be improved to reduce the probability of the critical event.

There are well-developed methods to establish a fault tree, so the modelling procedure will not be discussed in this paper. A fault tree can be easily converted to a Bayesian network \cite{bobbio-2001-p249-260, codetta-ra-2015-p13-24}. However, it is noted that, the conditional probability table contains more information than logic gate. In other words, the logic gate of fault tree cannot accurately describe the relationship between function sometimes. For instance, if the cooling function is invalid, there will be at a 50 percent likelihood of crash of host in the same cabinet. Actually, we cannot model this relationship by using the fault tree, but the Bayesian network can easily describe this relationship with conditional probability table. To model the dependency of functions more accurately, we analyze dependency of every function failure node in Bayesian network, and amend the corresponding conditional probability table. The conditional probability is obtained from historical data or expertise.

\subsubsection{Incident Level}
\label{sec:Incident Level}
In ICS, if an incident takes place, it may trigger other incidents. This phenomenon is called ``Domino Effect''. For instance, when the pressure of a reactor exceeds the safe threshold level, it is likely to cause the explosion accident. And even worse, the explosion may lead to casualties, environmental damages or property losses. We employ Bayesian network to model this relationship between incidents.

There are three steps to establish a Bayesian network of incidents. (1) Analyze historical data and consult engineers and experts, then identify all possible incident scenarios of ICS. (2) Analyze the causal relationship between incidents. Let $e$ represent an incident of ICS, if the occurrence of $e_i$ can cause the $e_j$, Bayesian network will add an arrow from $e_i$ to $e_j$, and $e_i$ is the parent node of $e_j$. (3) Generate the conditional probability table for each incident. Assume that there are $n$ parent nodes of $e_j$, Bayesian network uses a condition probability table, which is shown in \cref{tab:The Probabilities of Incident Occurrence}, to describe the probability of $e_j$.
\begin{table}[!ht]
    \centering
    \caption{The Probabilities of Incident Occurrence}\label{tab:The Probabilities of Incident Occurrence}
    \begin{tabu}to 0.85\textwidth{@{}X[-1,c]|*6{X[c]}@{}}
        $H(e_1)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        $H(e_2)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        \vd                 & \vd & \vd & \vd & \vd & $\ddots$ & \vd\\
        $H(e_n)$            & \TT & \FF & \TT & \TT & $\cdots$ & \FF\\\hline
        $H(e_j)$            & $h_{e_j,1}$   & $h_{e_j,2}$ & $h_{e_j,3}$ & $h_{e_j,4}$ & $\cdots$ & $h_{e_j,2^n}$\\
        $\overline{H}(e_j)$ & $1-h_{e_j,1}$ & $1-h_{e_j,2}$ & $1-h_{e_j,3}$ & $1-h_{e_j,4}$ & $\cdots$ & $1-h_{e_j,2^n}$\\
    \end{tabu}
\end{table}

In \cref{tab:The Probabilities of Incident Occurrence}, $H(e_j)$ represents that the incident $e_j$ has occurred, $\overline{H}(e_j)$ represents that the incident $e_j$ has not occurred. The first column of \cref{tab:The Probabilities of Incident Occurrence} shows that if all incidents $e_1,e_2,\cdots,e_n$ has occurred, the probability of $e_j$ is $h_{e_j,1}$. The probabilities $h_{e_j,k}$, where $k = 1,2,\cdots,2^n$, are based on historical data or expertise.

There exist overlaps between consequences of incidents. The loss of overlapped part will be calculated repeatedly, as a result, it will cause the error to a risk. To solve this problem, the consequences of incidents need to be decoupled. There are four steps to decouple consequences. First step, for each incident $e_i$, analyze its consequence and generate a consequence set $\bm{c}_i = (c_1,c_2,\cdots,c_n)$. The element of consequence set $\bm{c}_i$ could be field workers, facilities, environment, productions and etc. The meaning of $\bm{c}_i$ is that the occurring of incident $e_i$ will threaten the elements in consequence set $\bm{c}_i$. For instance, the incident $e_i$ is explosion of a reactor, it may cause casualties of workers, pollution of air, damage of facilities and losses of productions. The consequence set of $e_i$ is
\[
\bm{c}_i = (\text{workers},\text{air},\text{facilities},\text{productions}).
\]

Second step, generate $\bm{C}' = (\bm{c}_1',\bm{c}_2',\cdots,\bm{c}_{m'}')$ based on $\bm{C} = (\bm{c}_1, \bm{c}_2, \cdots, \bm{c}_m)$, and the following conditions must be met:
\begin{align}
    \text{Completeness} & :  \textstyle\bigcup_{i=1}^m \bm{c}_i= \bigcup_{i=1}^{m'} \bm{c}_i',
    \label{eqn:Condition 1 of C Completeness}\\
    \text{Independence} & :  \forall \bm{c}_i',\bm{c}_j' \in \bm{C}' \text{ : } \bm{c}_i' \cap \bm{c}_j' = \varnothing,
    \label{eqn:Condition 2 of C Independence}\\
    \text{Traceability} & : \forall \bm{c}' \in \bm{C}', \exists \bm{c} \in \bm{C} \text{ : } \bm{c}' \subseteq \bm{c}.
    \label{eqn:Condition 3 of C Traceability}
\end{align}

\Cref{alg:Decoupling Algorithm of C} shows a promotion algorithm, which can minimize the number of elements of $\bm{C}'$. Low number of elements of $\bm{C}'$ can reduce complexity of Bayesian network.
\begin{algorithm}[!ht]
    \caption{Decoupling Algorithm of $\bm{C}$}
    \label{alg:Decoupling Algorithm of C}
    \begin{multicols}{2}
    \begin{algorithmic}[1]
    \REQUIRE $\bm{C} = (\bm{c}_1,\bm{c}_2,\cdots,\bm{c}_{m})$
    \ENSURE $\bm{C}' = (\bm{c}_1',\bm{c}_2',\cdots,\bm{c}_{m'}')$

    \STATE $\bm{C}' \gets \varnothing$

    \FOR{$i = 1$ to $m$}
        \STATE $n \leftarrow \text{number of elements of } \bm{C}'$
        \FOR{$j = 1$ to $n$ }
            \STATE $\bm{t}_1 \gets \bm{c}_i \cap \bm{c}_j'$
            \STATE $\bm{t}_2 \gets \bm{c}_j' - \bm{t}_1$
            \STATE $\bm{c}_i \gets \bm{c}_i - \bm{t}_1$
            \FOR{$k = 1$ to $2$}
                \IF{$\bm{t}_k \neq \varnothing$}
                    \STATE Add $\bm{t}_k$ in end of $\bm{C}'$
                \ENDIF
            \ENDFOR
        \ENDFOR
        \IF{$\bm{c}_i \neq \varnothing$}
            \STATE Add $\bm{c}_i$ in end of $\bm{C}'$
        \ENDIF
    \ENDFOR
    \RETURN $\bm{C}'$
    \end{algorithmic}
    \end{multicols}
\end{algorithm}

Third step, for each $\bm{c}'_j$ in $\bm{C}'$, generate a corresponding auxiliary node $x_j$. According to the traceability of $\bm{C}'$ which is shown in \cref{eqn:Condition 3 of C Traceability}, there must be $\bm{c}_i \in\bm{C}$ that $\bm{c}_j' \subseteq\bm{c}_i$. Generate the incident set $\bm{e}_j$ for each $\bm{c}_j'$, which meets following conditions:
\begin{align}
  & \forall e_i \in \bm{e}_j, \bm{c}_j'\subseteq \bm{c}_i,
  \label{eqn:Condition 1 of Incident Set}\\
  & \nexists e_i \notin \bm{e}_j, \bm{c}_j'\subseteq \bm{c}_i.
  \label{eqn:Condition 2 of Incident Set}
\end{align}

Assume that the incident set of $\bm{c}_j'$ is $\bm{e}_j = (e_{i_1}, e_{i_2}, \cdots, e_{i_n})$, then add an auxiliary node $x_j$ in the Bayesian network. The parent nodes of the new auxiliary node $x_j$ are $e_{i_1}, e_{i_2}, \cdots, e_{i_n}$.

Fourth step, for each auxiliary node $x_j$, generate a conditional probability table. The conditional probability table of auxiliary node $x_j$ is shown in \cref{tab:The Conditional Probability Table of Auxiliary Node}.
\begin{table}[!ht]
    \centering
    \caption{The Conditional Probability Table of Auxiliary Node}\label{tab:The Conditional Probability Table of Auxiliary Node}
    \begin{tabu}to 0.85\textwidth{@{}X[-1,c]|*6{X[c]}@{}}
        $H(e_{i_1})$        & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        $H(e_{i_2})$        & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        \vd                 & \vd & \vd & \vd & \vd & $\ddots$ & \vd\\
        $H(e_{i_n})$        & \TT & \FF & \TT & \FF & $\cdots$ & \FF\\\hline
        $H(x_j)$            & $h_{x_j,1}$ & $h_{x_j,2}$ & $h_{x_j,3}$ & $h_{x_j,4}$ & $\cdots$ & $h_{x_j,2^{i_n}}$\\
        $\overline{H}(x_j)$ & $1-h_{x_j,1}$ & $1-h_{x_j,2}$ & $1-h_{x_j,3}$ & $1-h_{x_j,4}$ & $\cdots$ & $\mathclap{1-h_{x_j,2^{i_n}}}$
    \end{tabu}
\end{table}

The first column of \Cref{tab:The Conditional Probability Table of Auxiliary Node} shows that if $e_{i_1}, e_{i_2}, \cdots, e_{i_n}$ all occur, the probability that elements in the auxiliary node $x_i$ are damaged is $h_{x_j,1}$. $h_{x_j,k}$, where $k = 1,2,\cdots,2^{i_n}$, is based on historical data or expertise.

\subsubsection{Information Transfer between Levels}
\label{sec:Information Transfer between Levels}
The cyber attacks can lead to system function failures, and the function failures may cause the industrial incidents. To analyze the destructive effect propagation of cyber attacks, there must be information transfer between three aforementioned layers.

For system functions, besides the failures of their parent nodes, the cyber attack can also invalid them. For each function $f_i$ in the function model, find all attack nodes which may lead to failure of $f_i$ in attack model. Then add arrows from attack nodes to function node $f_i$. Assume that there are $n$ parent nodes of function $f_i$ and $m$ attack nodes may invalid function $f_i$, \cref{fig:Relationship between Function and Attack} shows the relationship diagram of function $f_i$.
\begin{figure}[!ht]
  \centering
  \input{Figures/RelationshipOfFunctionAndAttack}
  \caption{Relationship between Function and Attack}
  \label{fig:Relationship between Function and Attack}
\end{figure}

Then analyze all the situation of $f_1,f_2,\cdots,f_n$ and $a_1,a_2,\cdots,a_m$, and get the condition probability of failure of function $f_i$, which is shown in \cref{tab:The Probabilities of Function f i Failure}, by expertise or historical data.
\begin{table}[!ht]
    \centering
    \caption{The Probabilities of Function $f_i$ Failure}\label{tab:The Probabilities of Function f i Failure}
    \begin{tabu}to 0.85\textwidth{@{}X[-1,c]|*6{X[c]}@{}}
        $F(f_1)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        $F(f_2)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        \vd                 & \vd & \vd & \vd & \vd & $\ddots$ & \vd\\
        $F(f_n)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        $L(a_1)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        $L(a_2)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        \vd                 & \vd & \vd & \vd & \vd & $\ddots$ & \vd\\
        $L(a_m)$            & \TT & \FF & \TT & \FF & $\cdots$ & \FF\\
        \hline
        $F(f_i)$            & $b_{f_i,1}$ & $b_{f_i,2}$   & $b_{f_i,3}$   & $b_{f_i,4}$   & $\cdots$ & $\mathclap{b_{f_i,2^{m+n}}}$\\
        $\overline{F}(f_i)$ & $1-b_{f_i,1}$ & $1-b_{f_i,2}$   & $1-b_{f_i,3}$   & $1-b_{f_i,4}$   & $\cdots$ & $\mathclap{1-b_{f_i,2^{m+n}}}$\\
    \end{tabu}
\end{table}

Failure of system functions is a significant cause of industrial incidents. For instance, the failure of temperature control function may result in the incident that means temperature in reactor exceeds the threshold. For each incident $e_i$ in ICS, analyze all system functions that its failure can lead to the occurrence of incident $e_i$, then add arrows from function failure nodes to incident $e_i$. Assume that there are $n$ parent nodes of incident $e_i$ and $m$ function failure nodes may cause incident $e_i$, \cref{fig:Relationship between Incident and Function} shows the relationship diagram of incident $e_i$.
\begin{figure}[!ht]
  \centering
  \input{Figures/RelationshipOfIncidentAndFunction}
  \caption{Relationship between Function and Attack}
  \label{fig:Relationship between Incident and Function}
\end{figure}

Then analyze all the situation of $e_1,e_2,\cdots,e_n$ and $f_1,f_2,\cdots,f_m$, and get the conditional probability table of incident $e_i$, which is shown in \cref{tab:The Probabilities of Incident e i}, by historical data or expertise.
\begin{table}[!ht]
    \centering
    \caption{The Probabilities of Incident $e_i$}\label{tab:The Probabilities of Incident e i}
    \begin{tabu}to 0.85\textwidth{@{}X[-1,c]|*6{X[c]}@{}}
        $H(e_1)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        $H(e_2)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        \vd                 & \vd & \vd & \vd & \vd & $\ddots$ & \vd\\
        $H(e_n)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        $F(f_1)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        $F(f_2)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        \vd                 & \vd & \vd & \vd & \vd & $\ddots$ & \vd\\
        $F(f_m)$            & \TT & \TT & \TT & \TT & $\cdots$ & \FF\\
        \hline
        $H(e_i)$            & $h_{e_i,1}$ & $h_{e_i,2}$   & $h_{e_i,3}$   & $h_{e_i,4}$   & $\cdots$ & $\mathclap{h_{e_i,2^{m+n}}}$\\
        $\overline{H}(e_i)$ & $1-h_{e_i,1}$ & $1-h_{e_i,2}$   & $1-h_{e_i,3}$   & $1-h_{e_i,4}$   & $\cdots$ & $\mathclap{1-h_{e_i,2^{m+n}}}$\\
    \end{tabu}
\end{table}

\subsection{Incident Prediction}
\label{sec:Incident Prediction}
\subsubsection{Collection of Data and Evidence}
\label{sec:Collection of Data and Evidence}
An IDS is a device or software application that monitors network or system activities for malicious activities or policy violations and produces reports to a management station or risk assessment module. Anomaly detection system (ADS) collects data of system to compare with the normal value, if there is considerable deviation, like the IDS, ADS will generate a report to risk assessment module. In some researches about anomaly-based IDS, ADS is a part of anomaly-based IDS. In this paper, IDS represents the signature-based IDS which do not contain ADS, IDS and ADS is two separated system.

When IDS detects attacks, it generates attack evidences and send them to incident prediction module. Similar, ADS detects anomalies and send anomaly evidences to incident prediction module. For each attack evidence or anomaly evidence, we can find a unique corresponding node in multi-level Bayesian network.

As we all know, correlation only exists between the atom attacks in a combination attack. If two atom attacks do not belong to a combination attack, we will not get correct prediction by inferring multi-level Bayesian network with these two attacks. To solve this problem, $T$ as maximum interval of adjacent continuous atom attacks is proposed. If interval of adjacent continuous attacks is larger than $T$, multi-level Bayesian network do not regard these two attacks as a combination attack. The length of $T$ can be obtained by analyzing a significant volume of historical data of combination attacks.

Suppose that $\bm{E}_a$ is the set of attack evidences. The algorithm of updating evidences in Bayesian network is shown in \cref{alg:Updating Evidences E a}.
\begin{algorithm}[!ht]
    \caption{Update Evidences $\bm{E}_a$}
    \label{alg:Updating Evidences E a}
    \begin{multicols}{2}
    \begin{algorithmic}[1]
    \REQUIRE $\bm{E}_a, {\it LastTime}, e$
    \ENSURE $\bm{E}_a, {\it LastTime}$

    \IF{$\it Now - LastTime > T$}
        \STATE Clear $\bm{E}_a$
    \ENDIF

    \IF{$e = \text{\tt NULL}$}
        \RETURN
    \ENDIF

    \STATE Add $e$ into $\bm{E}_a$
    \STATE $\it LastTime \leftarrow Now$
    \end{algorithmic}
    \end{multicols}
\end{algorithm}

In \cref{alg:Updating Evidences E a}, the variable $\it Now$ is the current absolute time. To better illustrate the updating process of $\bm{E}_a$, an example of updating is shown in \cref{fig:An Example of Updating Evidences in Bayesian Network}.
\begin{figure}[!ht]
  \centering
  \input{Figures/ExampleOfUpdatingEvidencesInBayesianNetwork}
  \caption{An Example of Updating Evidences in Bayesian Network}
  \label{fig:An Example of Updating Evidences in Bayesian Network}
\end{figure}

Suppose that $\bm{E}_b$ is the set of anomaly evidences. If an anomaly evidence is added into $\bm{E}_b$, it exist until the corresponding anomaly is removed.

\subsubsection{Calculation of Incident Probability}
\label{sec:Calculation of Incident Probability}
Let $\bm{E} = \bm{E}_a \cup\bm{E}_b$. When any evidence in $\bm{E}$ is changed, remove all the mark in multi-level Bayesian network, then for each evidence in $\bm{E}$ find and mark the corresponding node. An example of marked multi-level Bayesian network is shown in \cref{fig:Multi-Level Bayesian Network with Evidences}.
\begin{figure}[!ht]
  \centering
  \input{Figures/MultiLevelBayesianNetworkWithEvidences}
  \caption{Multi-Level Bayesian Network with Evidences}
  \label{fig:Multi-Level Bayesian Network with Evidences}
\end{figure}

In \cref{fig:Multi-Level Bayesian Network with Evidences}, the nodes filled with black represent their corresponding event has occurred. For example, the node $a_6$ means that attacker has launched attack $a_6$, the node $f_6$ refers that the function $f_6$ has been failed, the node $e_5$ shows the incident $e_5$ has happened, etc.

Then the multi-level Bayesian network can be regarded as a whole Bayesian network, and can be inferred by the algorithm named probability propagation in trees of clusters (PPTC) \cite{huang-1996-p225-263}. With algorithm of PPTC, probabilities of all nodes in multi-level Bayesian network can be calculated.