ExecutionTime = load('./OutputData/ExecutionTime.dat');

DistributionOfExecutionTime = Functions.GetStatisticsByStep(ExecutionTime, 0.0005);
DistributionOfExecutionTime(:, 1) = 1000 * DistributionOfExecutionTime(:, 1);
DistributionOfExecutionTime(:, 2) = DistributionOfExecutionTime(:, 2)./size(ExecutionTime, 1);
save './OutputData/DistributionOfExecutionTime.dat' DistributionOfExecutionTime -ascii;
