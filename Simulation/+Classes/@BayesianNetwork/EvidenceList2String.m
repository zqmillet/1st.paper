function String = EvidenceList2String(obj)
    String = '';
    for i = 1:numel(obj.Nodes)
        if isequal(obj.Evidences{i}, Enumerations.EvidenceState.Unknown)
            continue;
        end
        
        if obj.Evidences{i} == Enumerations.EvidenceState.Happened
            String = Functions.ConcatenateString(String, '+', obj.Nodes{i}.Label, ',');      
        end
        
        if obj.Evidences{i} == Enumerations.EvidenceState.Unhappened
            String = Functions.ConcatenateString(String, '-', obj.Nodes{i}.Label, ',');      
        end
    end
    String = String(1:end - 1);
    if isempty(String)
        String = '---';
    end
end

