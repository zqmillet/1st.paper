function ClearAttackEvidences(obj)

    for i = 1:numel(obj.Nodes)
        if obj.Nodes{i}.Type == Enumerations.NodeType.Attack
            obj.RemoveEvidences(obj.Nodes{i});
        end
    end
end

