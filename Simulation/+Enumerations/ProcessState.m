classdef ProcessState
    properties (Constant)
        Normal = 1;
        Failed = 0;
    end
    
    methods (Access = private)
        function obj = ProcessState
        end
    end
end

